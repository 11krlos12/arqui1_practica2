# Practica 2 - Arquitectura computación 1
# USAC - GUATEMALA

## Conexión I2C, simulación en proteus + arduino



* Diagrama general
<div>
<p style = 'text-align:center;'>
<img src="./img/1.png" alt="JuveYell" width="1000px">
</p>
</div>

* Mensaje de autenticación de contraseña, error en contraseña. Esta se autentica al precionar la tecla "*" asterisco.
<div>
<p style = 'text-align:center;'>
<img src="./img/1.5.png" alt="JuveYell" width="400px">
</p>
</div>

* Mensaje de bienvenida, este aparece al ejecutar el programa
<div>
<p style = 'text-align:center;'>
<img src="./img/2.png" alt="JuveYell" width="500px">
</p>
</div>

* Ingreso de la contraseña correctamente, la cual es "202114" se autentica con la tecla asterisco "*"
<div>
<p style = 'text-align:center;'>
<img src="./img/3.png" alt="JuveYell" width="500px">
</p>
</div>


* Activación del ventilador, sensor de temperatura
Estos se activan en los rango (x) siguientes:
    *  Si es  x < 18 , ambos motores se encuentran apagados
    *  Si es  x > 18 && < 25, se enciende el motor "2do nivel de la casa"
    *  Si es x >= 25, ambos motores se encuentran encendidos
    *  La tecla "#" apaga ambos motores sin importar en que rango se encuentren
    *  Para moverlos de nuevo, se debe ingresar la contraseña nuevamente
<div>
<p style = 'text-align:center;'>
<img src="./img/4.png" alt="JuveYell" width="500px">
</p>
<p style = 'text-align:center;'>
<img src="./img/5.png" alt="JuveYell" width="500px">
</p>
<p style = 'text-align:center;'>
<img src="./img/6.png" alt="JuveYell" width="500px">
</p>
</div>

* Version de Proteus utilizada
<div>
<p style = 'text-align:center;'>
<img src="./img/7.png" alt="JuveYell" width="300px">
</p>
</div>


* Version de arduino utilizada 
<div>
<p style = 'text-align:center;'>
<img src="./img/8.png" alt="JuveYell" width="300px">
</p>
<p style = 'text-align:center;'>
<p><label > Librerias utilizadas y son adquiridas en el gestor de librerias de arduino </label></p>
<img src="./img/9.png" alt="JuveYell" width="500px">
</p>
</div>
