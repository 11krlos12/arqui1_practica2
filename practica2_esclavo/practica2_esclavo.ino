#include <Wire.h>
#include <LiquidCrystal.h>
#include <Servo.h>
#define DS1621_ADDRESS  0x48


/*
#-----------------------------------------------#
#                 Krlos López                   #
#                  201313894                    #
#             ♡ Debian + Gnome ♡                #
#-----------------------------------------------#
*/


int rangoMotores = -1;
LiquidCrystal lcd2(13,12,11,10,9,8);
Servo miServo;
boolean flag = false;


int i=0;
void setup() {
  Wire.begin(1);//declaramos que es esclavo "1"
  Serial.begin(9600);
  lcd2.begin(16,2);
  lcd2.print("APAGADO");

  
  rangoMotores = -1;
  Wire.onReceive(ejecucionComunicacionArduinoMaestro); //recibo valores del maestro
  
}
int angulo;

void loop() {

  if(rangoMotores==99){//motores apagados
    //Serial.print("motores apagados");
    if(flag == true){
      return;
    }
    
    digitalWrite(4,LOW);
    digitalWrite(6,LOW);
    lcd2.clear();
    lcd2.setCursor(0, 0);
    lcd2.print("APAGADO");
    flag = true;
    return;
  } else {
    flag = false;
  }

  
  if (rangoMotores > -1  && rangoMotores < 19)//la variable rangoMotores ====== la temperatura a mostrar en la lcd2
  {
    lcd2.setCursor(0, 0);
    lcd2.print("TEMP: ");
    lcd2.print(rangoMotores);
    lcd2.println(" C");

    lcd2.setCursor(0, 1);//uno significa fila 2 xD
    lcd2.println("NIVEL: 1");
    digitalWrite(4,LOW);
    //moverMotor1();
  }
  else if(rangoMotores > 18 && rangoMotores < 25)
  {
    lcd2.setCursor(0, 0);
    lcd2.print("TEMP: ");
    lcd2.print(rangoMotores);
    lcd2.println(" C");
    
    lcd2.setCursor(0, 1);
    lcd2.println("NIVEL: 2");
    digitalWrite(6,LOW);
    moverMotor2();
  }
  else if(rangoMotores >= 25)
  {
    lcd2.setCursor(0, 0);
    lcd2.print("TEMP: ");
    lcd2.print(rangoMotores);
    lcd2.println(" C");
    
    lcd2.setCursor(0, 1);
    lcd2.println("NIVEL: 3");
    moverMotor1();
    moverMotor2();
  }

  delay(250);
}



void ejecucionComunicacionArduinoMaestro()
{
  rangoMotores = Wire.read(); //recibe valores del maestro
  Serial.println("valor del rango");
  Serial.println(rangoMotores);
}

void moverMotor1()
{
  pinMode(6, OUTPUT);
  pinMode(5, OUTPUT);
  
  digitalWrite(6,HIGH);
  digitalWrite(5, LOW);
}

void moverMotor2()
{
  pinMode(4, OUTPUT);
  pinMode(3, OUTPUT);
  
  digitalWrite(4,HIGH);
  digitalWrite(3, LOW);  
}
