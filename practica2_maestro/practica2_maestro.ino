#include <Servo.h>
#include <Key.h>
#include <Keypad.h>
#include <LiquidCrystal.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#define DS1621_ADDRESS  0x48


/*
#-----------------------------------------------#
#                 Krlos López                   #
#                  201313894                    #
#             ♡ Debian + Gnome ♡                #
#-----------------------------------------------#
*/

//lcd
LiquidCrystal_I2C lcd1(0x20, 16, 2);

//teclado
const byte rows = 4;
const byte cols = 3;
char keys[rows][cols] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};
byte rowPins[rows] = {5, 4, 3, 2};
byte colPins[cols] = {6, 7, 8};
Keypad pad = Keypad(makeKeymap(keys), rowPins, colPins, rows, cols);

//variables y banderas globales
char caracterEntrada = '\0';
bool b_mensajeInicial = true;
String contrasena = "";
bool b_claveCorrecta;

int pararMotores = 0;

//vector para temperaturas
char c_buffer[8], f_buffer[8];


void setup() {

  Wire.begin();//declaracion del maestro -nada entre los parentesis-
  //LCD
  lcd1.begin(16, 2);
  lcd1.init();
  lcd1.backlight();


  //SENSOR
  Wire.beginTransmission(DS1621_ADDRESS); // connect to DS1621 (send DS1621 address)
  Wire.write(0xAC);                       // send configuration register address (Access Config)
  Wire.write(0);                          // perform continuous conversion
  Wire.beginTransmission(DS1621_ADDRESS); // send repeated start condition
  Wire.write(0xEE);                       // send start temperature conversion command
  Wire.endTransmission();


  //asignacion de valores de variables globales.
  caracterEntrada = '\0';
  b_mensajeInicial = true;
  b_claveCorrecta = false;
  contrasena = "";
  pararMotores = 0;
}

void loop() {
  caracterEntrada = pad.getKey();
  int16_t c_temp = (get_temperature() / 10); //capturo los grados centigrados

  //impresion del mensaje inicial
  if (caracterEntrada == NO_KEY && b_mensajeInicial)
  {
    lcd1.clear();
    lcd1.setCursor(0, 0);
    lcd1.println("CASA ACYE1");

    lcd1.setCursor(0, 1);   //posiciona el segundo mensaje en la segunda linea del lcd
    lcd1.print("B-G14-S2");
    b_mensajeInicial = false;
  }
  else if (caracterEntrada == '#') {
    b_mensajeInicial = true;
    c_temp = 99;
    contrasena = "#";
    pararMotores = 1;
  }
  else if (caracterEntrada != NO_KEY) //para ingresar la contraseña las veces necesarias.
  {
    lcd1.clear();
    lcd1.setCursor(0, 0);
    lcd1.print("CONTRASENA:");
    int i = 0; //corrimiento en la lcd
    while (true) {
      caracterEntrada = pad.getKey();
      if (caracterEntrada == '*') {
        b_mensajeInicial = true;
        break;
      }
      if (caracterEntrada != NO_KEY) {
        contrasena += caracterEntrada;
        lcd1.setCursor(i, 1);
        lcd1.print(caracterEntrada);
        i++;
      }
    }


    lcd1.clear();
    lcd1.setCursor(0, 0);
    if (contrasena == "202114")//contraseña correcta
    {
      lcd1.print("BIENVENIDO A");
      lcd1.setCursor(0, 1);
      lcd1.print("CASA");
      delay(50);
      //lcd1.clear();
      b_claveCorrecta = true;
      b_mensajeInicial = false;
      pararMotores = 0;
    }
    else if (contrasena != '#')                  //contraseña incorrecta
    {
      b_mensajeInicial = true;
      contrasena = "";
      lcd1.print("ERROR EN");
      lcd1.setCursor(0, 1);
      lcd1.print("CONTRASENA");
      delay(1000);

    }
  }//if


  //
  if (pararMotores != 1){
    if (b_claveCorrecta)
    {
      //lcd1.clear();
      //lcd1.setCursor(0, 0);
      temperaturas(c_temp);
      delay(100);
    }
  }else{
    temperaturas(99);
    delay(300);
  }




}

//envio de temperatura al esclavo
void temperaturas(int16_t temperatura)
{
  envioDatosEsclavo(temperatura);
}


void envioDatosEsclavo(int16_t temperatura)
{
  Wire.beginTransmission(1);
  Wire.write(temperatura);
  Wire.endTransmission();
}





//https://simple-circuit.com/arduino-ds1621-digital-temperature-sensor/
int16_t get_temperature() {
  Wire.beginTransmission(DS1621_ADDRESS); // connect to DS1621 (send DS1621 address)
  Wire.write(0xAA);                       // read temperature command
  Wire.endTransmission(false);            // send repeated start condition
  Wire.requestFrom(DS1621_ADDRESS, 2);    // request 2 bytes from DS1621 and release I2C bus at end of reading
  uint8_t t_msb = Wire.read();            // read temperature MSB register
  uint8_t t_lsb = Wire.read();            // read temperature LSB register

  // calculate full temperature (raw value)
  int16_t raw_t = (int8_t)t_msb << 1 | t_lsb >> 7;
  // convert raw temperature value to tenths °C
  raw_t = raw_t * 10 / 2;
  return raw_t;
}
